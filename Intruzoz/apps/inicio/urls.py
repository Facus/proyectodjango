from django.conf.urls import url, include
from .views import inicio, dos
from django.contrib.auth import views as authentication
""" aca importamos la vista encargada de la auth"""


urlpatterns = [
	# urlpath / met views / nombre
    url(r'^$', inicio, name='inicio'),
    url(r'^pag2/(?P<numero>[\d]{4})/$', dos, name='pag2'),
    url(r'^login/$', authentication.login,
    	{'template_name':'inicio/login.html'}, name='login'),
    url(r'^logout/$', authentication.logout_then_login, name='logout'),
]