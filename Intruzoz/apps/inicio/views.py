from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.

def inicio(request):
	context = {}
	context['nombre'] = 'Facus'
	context['persona'] = ['nico', 'tortosa', '25']
	return render (request, 'inicio/inicio.html', context)

def dos(request, numero):
	return render (request, 'inicio/pag2.html')

"""
class Inicio2(TemplateView):
	template_name = 'inicio/inicio.html'
	def get_context_data(self, **kwargs):
		ctx = super(Inicio2,self).get_context_data(**kwargs)
		ctx['persona'] = ['nati', 'nati', 25]
		return ctx
"""

#def login(request):
#	return render (request, 'inicio/login.html')