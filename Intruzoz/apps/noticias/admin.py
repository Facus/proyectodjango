from django.contrib import admin
from .models import Noticia, Comentario
# Register your models here.

class NoticiasAdmin(admin.ModelAdmin):
	search_fields = ('titulo',)
	list_display = (u'titulo', 'tipo', 'creado',)
	list_filter = ('tipo', 'creado')
	ordering = ['-creado']

admin.site.register(Noticia, NoticiasAdmin)

admin.site.register(Comentario)

