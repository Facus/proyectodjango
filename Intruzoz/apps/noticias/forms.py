from django import forms
from .models import Noticia


class Formulario(forms.ModelForm):
	class Meta:
		model = Noticia
		fields = '__all__'


class Contacto(forms.Form):
	nombre = forms.CharField(max_length=50, label='Tu nombre', required = True)
	email = forms.EmailField(label='Tu correo electronico', required = True)
	asunto = forms.CharField(max_length=50, label='Asunto', required = True)
	mensaje = forms.CharField(widget = forms.Textarea())




