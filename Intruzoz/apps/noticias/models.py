from django.db import models
from django.template.defaultfilters import slugify
from apps.core.models import TimeStampedModel
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.

class Noticia(TimeStampedModel):
	titulo = models.CharField(max_length=100,
							verbose_name='Title',
							help_text= 'Aca va el titulo',
				)
	resumen = models.CharField(max_length=200)
	cuerpo = RichTextUploadingField()
	TIPO_CHOICE = (
					('D', 'Deporte'),
					('P', 'Policiales'),
					('C', 'Cultural'),
				)
	tipo = models.CharField(max_length=1,
			choices = TIPO_CHOICE)
	slug = models.SlugField(editable=False, max_length=100)

	imagen = models.ImageField(upload_to = 'imagenes', blank = True)

	def save(self, *args, **kwargs):
		if not self.id:
			self.slug = slugify(self.titulo)
		super(Noticia, self).save(*args, **kwargs)

	def __str__(self):
		return self.titulo

from django.conf import settings
class Comentario(TimeStampedModel):
	cuerpo = models.CharField(max_length=1000)
	autor = models.ForeignKey(settings.AUTH_USER_MODEL)
	articulo = models.ForeignKey(Noticia)
	def __str__(self):
		return 'art-'+ str(self.articulo.id) +'-'+self.autor.username
