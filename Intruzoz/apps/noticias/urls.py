from django.conf.urls import url
from .views import (Listar_Noticias, Mostrar_Noticia, Editar_Noticia,
		 Eliminar_Noticia, Crear_Noticia, Contactar, Comentar_ajax)


urlpatterns = [
	# urlpath / met views / nombre
    url(r'^crear_noticia/', Crear_Noticia, name='crear_noticia'),
    url(r'^$', Listar_Noticias.as_view(), name='noticias'),
    url(r'^noticia/(?P<pk>[\d]+)/', Mostrar_Noticia.as_view(), name='noticia'),
    url(r'^Comentar_ajax/$', Comentar_ajax, name='comentar'),
    url(r'^editar_noticia/(?P<pk>[\d]+)/', Editar_Noticia.as_view(), name='editar_noticia'),
    url(r'^eliminar_noticia/(?P<pk>[\d]+)/', Eliminar_Noticia.as_view(), name='eliminar_noticia'),
    url(r'^contacto/', Contactar, name='contacto'),
]