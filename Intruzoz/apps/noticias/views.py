from django.shortcuts import render
from .models import Noticia, Comentario
#from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin


from .forms import Formulario, Contacto
from django.shortcuts import redirect
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required


# Create your views here.

"""
def noticias(request):
	entradas = Noticia.objects.all()
	return render (request, 'noticias/noticias.html', {'noticias' : entradas})

def mostrar_noticia(request, id_noticia):	
	articulo = Noticia.objects.get(pk = id_noticia)
	return render (request, 'noticias/noticia.html', {'noticia' : articulo})
"""

class Listar_Noticias(ListView):
	model = Noticia
	template_name = 'noticias/noticias.html'
	def get_queryset(request):
		respuesta = Noticia.objects.all().order_by('-creado')
		return respuesta

class Mostrar_Noticia(DetailView):
	model = Noticia
	template_name = 'noticias/noticia.html'
	def get_context_data(self, **kwargs):
		context = super(Mostrar_Noticia, self).get_context_data(**kwargs)
		context['comentarios'] = Comentario.objects.filter(articulo_id=self.object.id)
		return context

from django.utils import timezone
from django.http import HttpResponse
import json

@login_required
def Comentar_ajax(request):
	if request.is_ajax:
		cuerpo = request.GET.get("cuerpito",None)
		art = request.GET.get("art",None)
		posteo = Noticia.objects.get(id=art)
		autor = request.user
		dic={}
		print('si', cuerpo, art, posteo, '++')
		if cuerpo:
			print(cuerpo,art,autor,'--')
			Comentario.objects.create(cuerpo=cuerpo, autor=autor, articulo=posteo) 
			dic['cuerpo'] = cuerpo
			dic['autor'] = autor.username
			dic['creado'] = 'Ahora mismo'
			dic['bandera'] = True
		else:
			dic['bandera'] = False
		print (dic)

		return HttpResponse(json.dumps(dic), content_type="application/json")



@login_required
def Crear_Noticia(request):
	if request.method == 'POST':
		form = Formulario(request.POST)
		if form.is_valid():
			form.save()
			#post = form.save(commit=False)
			#post.author = request.user
			#post.published_date = timezone.now()
			#post.save()
			#return redirect('blog.views.post_detail', pk=post.pk)
			return redirect(reverse_lazy('noticias:noticias'))
		else:
			ctx={}
			ctx['form'] = form
			return render (request, 'noticias/crear_noticia.html', ctx)
	else:
		ctx={}
		ctx['form'] = Formulario()
	return render (request, 'noticias/crear_noticia.html', ctx)

class Editar_Noticia(LoginRequiredMixin, UpdateView):
	model = Noticia
	fields = ['titulo','resumen','cuerpo','tipo']
	template_name = 'noticias/editar_noticia.html'
	success_url= reverse_lazy('noticias:noticias')


class Eliminar_Noticia(LoginRequiredMixin, DeleteView):
	model = Noticia
	success_url = reverse_lazy('noticias:noticias')

def Contactar(request):
	if request.method == 'POST':
		form = Contacto(request.POST)
		if form.is_valid():
			Enviar_mail(form)
			return redirect(reverse_lazy('noticias:noticias'))
		else:
			ctx={}
			ctx['form'] = form
			return render (request, 'noticias/contacto.html', ctx)

	else:
		ctx={}
		ctx['form'] = Contacto()
	return render (request, 'noticias/contacto.html', ctx)

def Enviar_mail(datos):
	subject = datos.cleaned_data['asunto']
	content = datos.cleaned_data['mensaje']
	email = datos.cleaned_data['email']
	nombre = datos.cleaned_data['nombre']
	body = """Hola"""

	msg = EmailMessage(subject = subject, to = ['facustest@gmail.com'], body = body)
	msg.content_subtype = "html"
	msg.send()