from django.contrib import admin
from .models import Articulo, Proveedor, Venta, LineaVenta

# Register your models here.

admin.site.register(Articulo)

admin.site.register(Proveedor)

admin.site.register(Venta)

admin.site.register(LineaVenta)