from django import forms
from .models import LineaVenta, Venta

class FormVenta(forms.ModelForm):
	venta = forms.ModelChoiceField(queryset=Venta.objects.all())

	class Meta:
		model = LineaVenta
		exclude = ['venta']