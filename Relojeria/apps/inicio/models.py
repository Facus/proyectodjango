from django.db import models

class TimeStampedModel(models.Model):
	creado = models.DateTimeField(auto_now_add=True,
								verbose_name=u'creado',
								help_text=u'Fecha de creacion')
	modificado = models.DateTimeField(auto_now=True,
								verbose_name=u'modificado',
								help_text=u'Fecha de modificacion')

	class Meta:
		abstract = True


class Proveedor(models.Model):

	nombre = models.CharField(max_length=50)

	def __str__(self):
		return self.nombre


class Articulo(models.Model):

	nombre = models.CharField(max_length=50)

	precio = models.DecimalField(max_digits=14, decimal_places=2)

	descripcion = models.CharField(max_length=100)

	proveedor = models.ForeignKey(Proveedor)

	def __str__(self):
		return self.nombre


class Venta(TimeStampedModel):

	total = models.DecimalField(max_digits=14, decimal_places=2)

	def __str__(self):
		return 'Venta-'+str(self.id)

class LineaVenta(models.Model):

	venta = models.ForeignKey(Venta)

	articulo = models.ForeignKey(Articulo)

	cantidad = models.IntegerField()

	def __str__(self):
		return str(self.venta)+'-LineaVenta-'+str(self.id)
