from django.conf.urls import url
from .views import inicio, Listar_Articulos, Cargar_Venta, Ver_Proveedor, Lista_proveedores
# para importar la vista encargada de la auth ->
# from django.contrib.auth import views as authentication


urlpatterns = [
	# urlpath / met views / nombre
    url(r'^$', inicio, name='inicio'),
    url(r'^consulta/$', Listar_Articulos.as_view(), name='consulta'),
    url(r'^proveedor/$', Lista_proveedores.as_view(), name='proveedores'),
    url(r'^proveedor/(?P<pk>[\d]+)/', Ver_Proveedor.as_view(), name='proveedor'),
    url(r'^cargarventa/$', Cargar_Venta.as_view(), name='cargarventa'),
]