from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy

from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView

from .models import Articulo, Proveedor, Venta, LineaVenta
from .forms import FormVenta

# Create your views here.
def inicio(request):
	
	return render (request, 'inicio/inicio.html')

class Lista_proveedores(ListView):
	model = Proveedor
	template_name= 'inicio/proveedores.html'

class Listar_Articulos(ListView):
	model = Articulo
	context_object_name = 'articulos'
	template_name= 'inicio/consulta.html'
	def get_context_data(self, **kwargs):
		context = super(Listar_Articulos, self).get_context_data(**kwargs)

		dic = {}
		lista = []
		prec = {}
		tot = []
		for i in context['object_list']:
			dic[i.nombre] = LineaVenta.objects.filter(articulo_id=i.id)
			lista.append(dic[i.nombre])
			cant = 0
			for d in dic[i.nombre]:
				print('||',d.cantidad,'||')
				cant = cant + d.cantidad
			print (dic[i.nombre],'|', cant*i.precio)
			prec[i.nombre] = cant*i.precio
			tot.append(prec[i.nombre])
			print ('::',tot,'::')
		context['ventas'] = lista
		context['total'] = prec
		return context

class Ver_Proveedor(DetailView):
	model = Proveedor
	context_object_name = 'proveedor'
	template_name= 'inicio/proveedor.html'

	def get_context_data(self, **kwargs):
		context = super(Ver_Proveedor, self).get_context_data(**kwargs)
		print (context['proveedor'].id)
		context['articulos'] = Articulo.objects.filter(proveedor=context['proveedor'].id)


		return context


class Cargar_Venta(FormView):
	template_name= 'inicio/cargarventa.html'
	form_class = FormVenta
	success_url = reverse_lazy('inicio:inicio')
	
	def form_valid(self, form):
		# This method is called when valid form data has been POSTed.
		# It should return an HttpResponse.
		venta = form.save(commit=False)
		print ('Exito', venta.articulo)
		return super(Cargar_Venta, self).form_valid(form)